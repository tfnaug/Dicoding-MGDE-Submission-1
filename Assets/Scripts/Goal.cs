﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour 
{
	[HideInInspector] public Paddle Owner;

	private void OnTriggerEnter(Collider collison) 
	{
		Debug.Log("Hit");
		if(collison.gameObject.tag == "Hockey")
			Owner.Life--;
	}



}

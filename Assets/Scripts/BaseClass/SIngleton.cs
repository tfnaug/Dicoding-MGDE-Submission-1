using UnityEngine;

[System.Serializable]
public abstract class Singleton<T> : MonoBehaviour where T : Component
{
	private static T instance;
	public static T Instance
	{ 
		get
		{
			if(instance == null)
			{
				var obj = new GameObject();
				obj.hideFlags = HideFlags.HideAndDontSave;
				instance = obj.AddComponent<T>();
			}
			return instance;
		} 
		set
		{
			instance = value;
		} 
	}
}

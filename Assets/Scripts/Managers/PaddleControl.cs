﻿using UnityEngine;
using UnityEngine.UI;

public class PaddleControl : MonoBehaviour 
{
	public GameObject Left;
	public GameObject Rigth;
	public float paddleSpeed;

	public LeftPaddle LeftPaddle { get; set; }
	public RigthPaddle RigthPaddle { get; set; }

	public GameObject LeftCounterObject;
	public GameObject RigthCounterObject;
	private Text LeftCounterLabel;
	private Text RigthCounterLabel;

	public GameObject LeftGoalObject;
	public GameObject RigthGoalObject;
	private Goal leftGoal;
	private Goal rightGoal; 

	[HideInInspector]
	public bool IsPlaying { get; set; }

	private void Awake() 
	{
		Left.AddComponent<LeftPaddle>();
		Rigth.AddComponent<RigthPaddle>();		
	}

	private void Start () 
	{
		LeftPaddle = Left.GetComponent<LeftPaddle>();
		RigthPaddle = Rigth.GetComponent<RigthPaddle>();
		LeftPaddle.LifeHUD = LeftCounterObject;
		RigthPaddle.LifeHUD = RigthCounterObject;
		LeftCounterLabel = LeftCounterObject.GetComponent<Text>();
		RigthCounterLabel = RigthCounterObject.GetComponent<Text>();

		LeftPaddle.Speed = paddleSpeed;
		RigthPaddle.Speed = paddleSpeed;

		leftGoal = LeftGoalObject.GetComponent<Goal>();
		rightGoal = RigthGoalObject.GetComponent<Goal>();
		leftGoal.Owner = LeftPaddle as Paddle;
		rightGoal.Owner = RigthPaddle as Paddle; 

	}
	
	private void Update ()
    {
        if(IsPlaying)
            PlayGame();

        HealthCounterDisplay();

    }

    private void HealthCounterDisplay()
    {
        LeftCounterLabel.text = Printer(LeftPaddle.Life);
        RigthCounterLabel.text = Printer(RigthPaddle.Life);
    }

    private void PlayGame()
    {
        LeftPaddle.UsePaddle();
        RigthPaddle.UsePaddle();
        LeftPaddle.DebugRaycast();
        RigthPaddle.DebugRaycast();
    }

	private string Printer(int life)
		=> $"Health: {life}";

}

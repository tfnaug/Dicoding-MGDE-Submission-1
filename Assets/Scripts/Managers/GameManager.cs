﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
	public delegate void PlayDelegate();
	public PlayDelegate refreshHockey;

	[HideInInspector]
	public bool IsGameStart;
	[HideInInspector]
	public bool InGame;
	private bool isPauseCache;

	public GameObject TitlePanel;
	public GameObject QuitPanel;
	public GameObject PausePanel;
	public GameObject HUDPanel;
	public GameObject WinnerPanel;
	private Text winner;
	private bool togglePause;
	private bool toQuit;
	private bool holdScreen;
	private GameObject hockey;

	private PaddleControl paddleControl;

	private void Awake() 
	{
		refreshHockey = FindObjectOfType<Hockey>().Spawn;
		paddleControl = FindObjectOfType<PaddleControl>();
		paddleControl.IsPlaying = false;
		winner = WinnerPanel.transform.Find("Winner").gameObject.GetComponent<Text>();
		hockey = GameObject.Find("Hockey");
	}

	private	void Start () 
		=> HockeyDisable();
	
	private	void Update ()
    {
        if (!IsGameStart && !isPauseCache && !toQuit)
            StartGame();

        PanelFlow();

		WeGotOurWinner();
		RestartGame();

    }

	private void HockeyEnable()
		=> hockey.tag = "Hockey";

	private void HockeyDisable()
		=> hockey.tag = "Null";

	#region Win-Restart-Set
	private void ResetGame()
	{
		paddleControl.LeftPaddle.Life = 5;
		paddleControl.RigthPaddle.Life = 5;
		Time.timeScale = 1;
		refreshHockey();
		holdScreen = false;
	}

	private void WeGotOurWinner()
	{
		if(paddleControl.LeftPaddle.Life == 0)
			Winner("Blue");
		
		if(paddleControl.RigthPaddle.Life == 0)
			Winner("Orange");
	}

	private void Winner(string label)
	{
		winner.text = $"{label} Win!";
		WinnerPanel.SetActive(true);		
		Time.timeScale = 0;
		holdScreen = true;
	}

	private void RestartGame()
	{
		if(holdScreen && Input.GetKeyDown(KeyCode.Return))
		{
			ResetGame();
			WinnerPanel.SetActive(false);
		}
	}
	#endregion

	#region UI-Panel;
		
    private void PanelFlow()
    {
        ToggleEsc();
        Pause();
        Resume();
        ToMainMenu();
		Quit();
    }

    private void StartGame()
	{
		bool ready = 
			TitlePanel.activeInHierarchy && 
			Input.GetKeyDown(KeyCode.Return) &&
			!isPauseCache;

		if(ready)
		{
			refreshHockey();
			InGame = true;
			PanelToActive(TitlePanel, false);
			HUDPanel.SetActive(true);
			HockeyEnable();
		}
	}

	private void PanelToActive(GameObject panel, bool statement)
	{
		panel.SetActive(statement);
		paddleControl.IsPlaying = !statement;
	}

	private void ToggleEsc()
	{
		if(Input.GetKeyDown(KeyCode.Escape) && InGame)
			togglePause = !togglePause;
	}

	private void Pause()
	{
		if(togglePause)
		{
			Time.timeScale = 0;
			PanelToActive(PausePanel, true);
			isPauseCache = true;
			HockeyDisable();
		}
	}

	private void Resume()
	{
		if(isPauseCache && !togglePause)
		{
			Time.timeScale = 1;
			PanelToActive(PausePanel, false);
			isPauseCache = false;
			HockeyEnable();
		}
	}

	private void ToMainMenu()
	{
		bool toMenu = isPauseCache &&
			Input.GetKeyDown(KeyCode.Return);

		if(toMenu)
		{
			togglePause = false;
			PanelToActive(TitlePanel, true);
			PanelToActive(PausePanel, false);
			HUDPanel.SetActive(false);
			refreshHockey();
			InGame = false;
			IsGameStart = false;
			isPauseCache = false;
			HockeyDisable();
			Time.timeScale = 1;
		}
	}

	private void Quit()
	{
		if(!InGame && Input.GetKeyDown(KeyCode.Escape))
		{
			TitlePanel.SetActive(true);
			QuitPanelToggle(true);
		}

		if(toQuit && Input.GetKeyDown(KeyCode.Return))
		{
			Application.Quit();
			Debug.Log("Quit");
		}
		
		if(toQuit && Input.GetKeyDown(KeyCode.Space))
		{
			QuitPanelToggle(false);
			HUDPanel.SetActive(true);			

		}

	}

	private void QuitPanelToggle(bool statement)
	{
		TitlePanel.transform.Find("Enter").gameObject.SetActive(!statement);
		QuitPanel.SetActive(statement);
		toQuit = statement;	
	}
	#endregion

}

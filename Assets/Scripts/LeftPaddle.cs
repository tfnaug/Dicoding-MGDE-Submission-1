using UnityEngine;

public class LeftPaddle : Paddle
{

    protected override float PaddleInput()
    {
   		float inputValue = Speed * Time.deltaTime;
		bool toUp = Input.GetKey(KeyCode.W);
		bool toDown = Input.GetKey(KeyCode.S);

		if((toUp && !toDown) && NotTopStuck())
			return inputValue;
		else if((toDown && !toUp) && NotBottomStuck())
			return -inputValue;
		else
			return 0f;     
    }



}
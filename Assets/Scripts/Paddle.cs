﻿using UnityEngine;

public abstract class Paddle : MonoBehaviour 
{
    public Transform paddleTransform;
    [HideInInspector] public int Life = 5;
    [HideInInspector] public GameObject LifeHUD;
    [HideInInspector] public GameObject Goal;
    public float Speed { get; set; }
    protected Vector3 paddleVector;
    protected abstract float PaddleInput();

    protected virtual void Start()
        =>  paddleTransform = GetComponent<Transform>();

    protected virtual Vector3 PaddleMotion(float input)
        => new Vector3(
            paddleTransform.position.x,
            paddleTransform.position.y + input,
            paddleTransform.position.z);

    public void UsePaddle()
        =>  paddleTransform.position = PaddleMotion(PaddleInput());


    private float raycastOffset = 3f;
    private enum Direction 
    {
        Up = 1, 
        Down = -1
    }
    private bool IsHit(Direction direction)
    {
        RaycastHit hit;
        Vector3 raycastDirection = transform.up * (int)direction;
        float offset = transform.up.y * raycastOffset;
        Debug.DrawRay(transform.position, raycastDirection * offset, Color.red);
		return Physics.Raycast(transform.position, raycastDirection, out hit, offset);

    }
    
    protected bool NotTopStuck()
        => !IsHit(Direction.Up);

    protected bool NotBottomStuck()
        => !IsHit(Direction.Down);

    public void DebugRaycast()
    {
        Vector3 dir = transform.up * transform.up.y;
        Debug.DrawRay(transform.position, dir * raycastOffset, Color.red);
        Debug.DrawRay(transform.position, dir * -1 * raycastOffset, Color.blue);

    }

}

using UnityEngine;

public class RigthPaddle : Paddle
{
    protected override float PaddleInput()
    {
		float inputValue = Speed * Time.deltaTime;
		bool toUp = Input.GetKey(KeyCode.UpArrow);
		bool toDown = Input.GetKey(KeyCode.DownArrow);

		if((toUp && !toDown) && NotTopStuck())
			return inputValue;
		else if((toDown && !toUp) && NotBottomStuck())
			return -inputValue;
		else
			return 0f;  
    }   

}